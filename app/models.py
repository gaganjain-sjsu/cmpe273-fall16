#!/usr/bin/env python
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

app = Flask(__name__)

DATABASE = 'expenseDB'
PASSWORD = 'password'
USER = 'root'
HOSTNAME = 'mysqlserver'

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s/%s'%(USER, PASSWORD, HOSTNAME, DATABASE)
db = SQLAlchemy(app)

class CreateDB():
	def __init__(self):
		engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME))
		engine.execute("CREATE DATABASE IF NOT EXISTS %s "%(DATABASE))

class Expense(db.Model):
	id = db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(80))
	email = db.Column(db.String(80))
	category = db.Column(db.String(80))
	description = db.Column(db.String(80))
	link = db.Column(db.String(150))
	estimated_costs = db.Column(db.String(10))
	submit_date = db.Column(db.String(80))
	status = db.Column(db.String(80))
	decision_date = db.Column(db.String(80))

	def __init__(self,name,email,category,description,link,estimated_costs,submit_date,status,decision_date):
		self.name= name
		self.email = email
		self.category = category
		self.description = description
		self.link = link
		self.estimated_costs = estimated_costs
		self.submit_date = submit_date
		self.status = status
		self.decision_date = decision_date

	@property
	def serialize(self):
		return {
			'id' : self.id,
			'name' : self.name,
			'email' : self.email,
			'category' : self.category,
			'description' : self.description,
			'link': self.link,
			'estimated_costs' : self.estimated_costs,
			'submit_date' : self.submit_date,
			'status' : self.status,
			'decision_date' : self.decision_date
		}

	"""def setKey(self,key,value):
		if( key == 'name'):
			self.name = value
		elif (key == 'email'):
			self.email = value
		elif (key == 'category'):
			self.category = value
		elif (key == 'description'):
			self.description = value
		elif (key == 'link'):
			self.link = value
		elif (key == 'estimated_costs'):
			self.estimated_costs = value"""
		