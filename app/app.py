#!/usr/bin/env python
 

from flask import Flask,jsonify,request,json
from models import db,Expense,CreateDB

app = Flask(__name__)

@app.before_first_request
def init_request():
	CreateDB()
	db.create_all()	

@app.route('/v1/expenses', methods=['POST'])
def postExpenses():

	req = request.get_json(force=True)
	status = "pending"
	decision_date = ""
	

	expense = Expense(req['name'],req['email'],req['category'],req['description'],req['link'],req['estimated_costs'],req['submit_date'],status,decision_date)
	db.session.add(expense)
	db.session.commit()
	resp = jsonify(expense.serialize)
	resp.status_code = 201
	return resp


@app.route('/v1/expenses/<int:expense_id>', methods=['GET','PUT','DELETE'])
def getExpense(expense_id):
	if(request.method == 'GET'):
		getExpense = Expense.query.filter_by(id =expense_id).first_or_404()
		return jsonify(getExpense.serialize)
	
	elif(request.method == 'DELETE'):
		deleteExpense = Expense.query.filter_by(id = expense_id).first_or_404()
		db.session.delete(deleteExpense)
		db.session.commit()
		resp = jsonify({'Status' : 'True'})
		resp.status_code = 204
		return resp

	else:
		putExpense = Expense.query.filter_by(id = expense_id).first_or_404()
		req = request.get_json(force=True)

		putExpense.estimated_costs = req['estimated_costs']
		#Code for any field update:
		#for key in request.json:
		#		putExpense.setKey(key,request.json[key])
		db.session.commit()
		resp = jsonify({'Status' : 'True'})
		resp.status_code = 202
		return resp


if __name__ == "__main__":
	app.run(host="0.0.0.0", port=8082, debug=True)